package ru.tsc.felofyanov.tm.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class DataLoadXmlJaxbRequest extends AbstractUserRequest {

    public DataLoadXmlJaxbRequest(@Nullable String token) {
        super(token);
    }
}
