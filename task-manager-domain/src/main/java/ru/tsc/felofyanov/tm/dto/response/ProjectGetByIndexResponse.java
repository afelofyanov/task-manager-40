package ru.tsc.felofyanov.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.model.Project;

@NoArgsConstructor
public final class ProjectGetByIndexResponse extends AbstractProjectResponse {

    public ProjectGetByIndexResponse(@Nullable Project project) {
        super(project);
    }
}
