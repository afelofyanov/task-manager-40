package ru.tsc.felofyanov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandService {

    void add(@NotNull AbstractCommand command);

    @Nullable
    AbstractCommand getCommandByName(@Nullable String name);

    @NotNull
    Iterable<AbstractCommand> getCommandsWithArgument();

    @Nullable
    AbstractCommand getCommandByArgument(@Nullable String argument);

    @NotNull
    Collection<AbstractCommand> getCommands();
}
