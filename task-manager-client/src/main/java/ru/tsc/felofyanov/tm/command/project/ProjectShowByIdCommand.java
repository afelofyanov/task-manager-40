package ru.tsc.felofyanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.dto.request.ProjectGetByIdRequest;
import ru.tsc.felofyanov.tm.dto.response.ProjectGetByIdResponse;
import ru.tsc.felofyanov.tm.model.Project;
import ru.tsc.felofyanov.tm.util.TerminalUtil;

public final class ProjectShowByIdCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-show-by-id";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show project by id.";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();

        @NotNull ProjectGetByIdRequest request = new ProjectGetByIdRequest(getToken(), id);
        @NotNull ProjectGetByIdResponse response = getServiceLocator().getProjectEndpoint().getProjectById(request);
        @Nullable final Project project = response.getProject();
        showProject(project);
    }
}
