package ru.tsc.felofyanov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.tsc.felofyanov.tm.dto.request.ServerAboutRequest;
import ru.tsc.felofyanov.tm.dto.response.ServerAboutResponse;

public final class AboutCommand extends AbstractSystemCommand {

    @NotNull
    @Override
    public String getName() {
        return "about";
    }

    @NotNull
    @Override
    public String getArgument() {
        return "-a";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show developer info.";
    }

    @Override
    public void execute() {
        System.out.println("[About]");
        System.out.println("[Client]");
        System.out.println("Name: " + getPropertyService().getAuthorName());
        System.out.println("E-mail: " + getPropertyService().getAuthorEmail());
        System.out.println("\n[Server]");
        @NotNull final ServerAboutResponse response =
                getServiceLocator().getSystemEndpoint().getAbout(new ServerAboutRequest());
        System.out.println("Name: " + response.getName());
        System.out.println("E-mail: " + response.getEmail());
    }
}
