package ru.tsc.felofyanov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.dto.request.UserProfileRequest;
import ru.tsc.felofyanov.tm.dto.response.UserProfileResponse;
import ru.tsc.felofyanov.tm.enumerated.Role;
import ru.tsc.felofyanov.tm.exception.entity.UserNotFoundException;
import ru.tsc.felofyanov.tm.model.User;

public final class UserViewProfileCommand extends AbstractUserCommand {

    @NotNull
    @Override
    public String getName() {
        return "user-view-profile";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "View profile of current user";
    }

    @Nullable
    @Override
    public Role @Nullable [] getRoles() {
        return Role.values();
    }

    @Override
    public void execute() {
        System.out.println("[VIEW USER PROFILE]");
        @NotNull final UserProfileResponse response =
                getServiceLocator().getUserEndpoint().viewProfileUser(new UserProfileRequest(getToken()));
        @Nullable final User user = response.getUser();
        if (user == null) throw new UserNotFoundException();
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("FIST NAME: " + user.getFirstName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("EMAIL: " + user.getEmail());
        System.out.println("ROLE: " + user.getRole());
    }
}
