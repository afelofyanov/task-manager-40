package ru.tsc.felofyanov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.felofyanov.tm.api.endpoint.IAuthEndpoint;
import ru.tsc.felofyanov.tm.api.endpoint.IUserEndpoint;
import ru.tsc.felofyanov.tm.dto.request.*;
import ru.tsc.felofyanov.tm.dto.response.*;
import ru.tsc.felofyanov.tm.enumerated.Role;
import ru.tsc.felofyanov.tm.model.User;

public class UserEndpointTest {

    @NotNull
    private final IUserEndpoint endpoint = IUserEndpoint.newInstance();

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance();

    @Nullable
    private String tokenAdmin;

    @Nullable
    private String tokenTest;

    @Before
    public void init() {
        @NotNull final UserLoginResponse adminResponse =
                authEndpoint.login(new UserLoginRequest("admin", "admin"));
        tokenAdmin = adminResponse.getToken();

        @NotNull final UserLoginResponse testResponse =
                authEndpoint.login(new UserLoginRequest("test", "test"));
        tokenTest = testResponse.getToken();
    }

    @Test
    public void changePassword() {
        Assert.assertThrows(Exception.class, () -> endpoint.changePassword(new UserChangePasswordRequest()));
        Assert.assertThrows(Exception.class, () -> endpoint.changePassword(
                new UserChangePasswordRequest("qwe", null)
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.changePassword(
                new UserChangePasswordRequest(tokenTest, "")
        ));

        UserChangePasswordResponse response = endpoint.changePassword(
                new UserChangePasswordRequest(tokenTest, "tester"));
        Assert.assertNotNull(response.getUser());

        UserLoginResponse successTest =
                authEndpoint.login(new UserLoginRequest("test", "tester"));
        Assert.assertTrue(successTest.getSuccess());

        UserChangePasswordResponse undo = endpoint.changePassword(
                new UserChangePasswordRequest(tokenTest, "test"));
        Assert.assertNotNull(undo.getUser());
    }

    @Test
    public void lockUser() {
        Assert.assertThrows(Exception.class, () -> endpoint.lockUser(new UserLockRequest()));
        Assert.assertThrows(Exception.class, () -> endpoint.lockUser(new UserLockRequest(null, null)));
        Assert.assertThrows(Exception.class, () -> endpoint.lockUser(new UserLockRequest("", null)));
        Assert.assertThrows(Exception.class, () -> endpoint.lockUser(new UserLockRequest("123", null)));
        Assert.assertThrows(Exception.class, () -> endpoint.lockUser(new UserLockRequest(tokenTest, "123")));
        Assert.assertThrows(Exception.class, () -> endpoint.lockUser(new UserLockRequest(tokenTest, "123")));
        Assert.assertThrows(Exception.class, () -> endpoint.lockUser(new UserLockRequest(tokenTest, "test")));

        UserLockResponse response = endpoint.lockUser(new UserLockRequest(tokenAdmin, "test"));
        Assert.assertNotNull(response);

        UserProfileResponse viewUser = endpoint.viewProfileUser(new UserProfileRequest(tokenTest));
        Assert.assertNotNull(viewUser.getUser());
        Assert.assertTrue(viewUser.getUser().getLocked());
        endpoint.unlockUser(new UserUnlockRequest(tokenAdmin, "test"));
    }

    @Test
    public void registryUser() {
        Assert.assertThrows(Exception.class, () -> endpoint.registryUser(new UserRegistryRequest()));
        Assert.assertThrows(Exception.class, () -> endpoint.registryUser(new UserRegistryRequest(
                null, "login", "pass", "email", Role.USUAL)
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.registryUser(new UserRegistryRequest(
                "", "login", "pass", "email", Role.USUAL)
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.registryUser(new UserRegistryRequest(
                "qwe", "login", "pass", "email", Role.USUAL)
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.registryUser(new UserRegistryRequest(
                tokenTest, null, "pass", "email", Role.USUAL)
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.registryUser(new UserRegistryRequest(
                tokenTest, "login", null, "email", Role.USUAL)
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.registryUser(new UserRegistryRequest(
                tokenTest, "login", "pass", null, Role.USUAL)
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.registryUser(new UserRegistryRequest(
                tokenTest, "login", "pass", "email", Role.USUAL)
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.registryUser(new UserRegistryRequest(
                tokenTest, "Another", "pass", "email", Role.USUAL)
        ));

        UserRegistryResponse response =
                endpoint.registryUser(new UserRegistryRequest(tokenTest, "registryUser", "pass", "emails", Role.USUAL));
        Assert.assertNotNull(response.getUser());

        @Nullable User user = response.getUser();
        Assert.assertNotNull(user.getLogin());

        UserLoginResponse login = authEndpoint.login(new UserLoginRequest("registryUser", "pass"));
        Assert.assertTrue(login.getSuccess());
    }

    @Test
    public void removeUser() {
        Assert.assertThrows(Exception.class, () -> endpoint.removeUser(new UserRemoveRequest()));
        Assert.assertThrows(Exception.class, () -> endpoint.removeUser(new UserRemoveRequest(null, null)));
        Assert.assertThrows(Exception.class, () -> endpoint.removeUser(new UserRemoveRequest("", null)));
        Assert.assertThrows(Exception.class, () -> endpoint.removeUser(new UserRemoveRequest("qwe", null)));
        Assert.assertThrows(Exception.class, () -> endpoint.removeUser(new UserRemoveRequest(tokenTest, null)));
        Assert.assertThrows(Exception.class, () -> endpoint.removeUser(new UserRemoveRequest(tokenAdmin, null)));
        Assert.assertThrows(Exception.class, () -> endpoint.removeUser(new UserRemoveRequest(tokenAdmin, "")));

        UserLoginResponse login = authEndpoint.login(new UserLoginRequest("registryUser", "pass"));
        Assert.assertTrue(login.getSuccess());

        UserRemoveResponse remove = endpoint.removeUser(new UserRemoveRequest(tokenAdmin, "registryUser"));
        Assert.assertNotNull(remove);
        Assert.assertThrows(Exception.class, () -> authEndpoint.login(new UserLoginRequest("registryUser", "pass")));
    }

    @Test
    public void unlockUser() {
        Assert.assertThrows(Exception.class, () -> endpoint.unlockUser(new UserUnlockRequest()));
        Assert.assertThrows(Exception.class, () -> endpoint.unlockUser(new UserUnlockRequest(null, null)));
        Assert.assertThrows(Exception.class, () -> endpoint.unlockUser(new UserUnlockRequest("", null)));
        Assert.assertThrows(Exception.class, () -> endpoint.unlockUser(new UserUnlockRequest("123", null)));
        Assert.assertThrows(Exception.class, () -> endpoint.unlockUser(new UserUnlockRequest(tokenTest, "123")));
        Assert.assertThrows(Exception.class, () -> endpoint.unlockUser(new UserUnlockRequest(tokenTest, "123")));
        Assert.assertThrows(Exception.class, () -> endpoint.unlockUser(new UserUnlockRequest(tokenTest, "test")));

        UserUnlockResponse response = endpoint.unlockUser(new UserUnlockRequest(tokenAdmin, "test"));
        Assert.assertNotNull(response);

        UserProfileResponse viewUser = endpoint.viewProfileUser(new UserProfileRequest(tokenTest));
        Assert.assertNotNull(viewUser.getUser());
        Assert.assertFalse(viewUser.getUser().getLocked());
    }

    @Test
    public void viewProfileUser() {
        Assert.assertThrows(Exception.class, () -> endpoint.viewProfileUser(new UserProfileRequest()));
        Assert.assertThrows(Exception.class, () -> endpoint.viewProfileUser(new UserProfileRequest("")));
        Assert.assertThrows(Exception.class, () -> endpoint.viewProfileUser(new UserProfileRequest("qwe")));

        UserProfileResponse viewUser = endpoint.viewProfileUser(new UserProfileRequest(tokenTest));
        Assert.assertNotNull(viewUser.getUser());

        @Nullable User user = viewUser.getUser();
        Assert.assertNotNull(user.getLogin());
    }

    @Test
    public void updateProfileUser() {
        Assert.assertThrows(Exception.class, () -> endpoint.updateProfileUser(new UserUpdateProfileRequest()));
        Assert.assertThrows(Exception.class, () -> endpoint.updateProfileUser(
                new UserUpdateProfileRequest(null, "first", "last", "middle")
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.updateProfileUser(
                new UserUpdateProfileRequest("", "first", "last", "middle")
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.updateProfileUser(
                new UserUpdateProfileRequest("234", "first", "last", "middle")
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.updateProfileUser(
                new UserUpdateProfileRequest(tokenTest, null, "last", "middle")
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.updateProfileUser(
                new UserUpdateProfileRequest(tokenTest, "first", null, "middle")
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.updateProfileUser(
                new UserUpdateProfileRequest(tokenTest, "first", "last", null)
        ));

        UserUpdateProfileResponse response = endpoint.updateProfileUser(new UserUpdateProfileRequest(
                tokenTest, "update", "profile", "user"
        ));
        Assert.assertNotNull(response.getUser());

        @Nullable User user = response.getUser();
        Assert.assertNotNull(user.getLogin());
        Assert.assertEquals("profile", user.getLastName());

        endpoint.updateProfileUser(new UserUpdateProfileRequest(
                tokenTest, "user", "return", "value"
        ));
    }
}
