package ru.tsc.felofyanov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.enumerated.Sort;
import ru.tsc.felofyanov.tm.model.Project;

import java.util.Collection;
import java.util.List;

public interface IProjectRepository extends IUserOwnerRepository<Project> {

    @Insert("INSERT INTO tm_project (id, name, description, user_id, status, created, start_dt, end_dt) " +
            " VALUES (#{id}, #{name}, #{description}, #{userId}, #{status}, #{created}, #{dateBegin}, #{dateEnd})")
    void add(@Nullable Project model);

    @Insert("INSERT INTO tm_project (id, name, description, user_id, status, created, start_dt, end_dt) " +
            " VALUES (#{id}, #{name}, #{description}, #{userId}, #{status}, #{created}, #{dateBegin}, #{dateEnd})")
    void addAll(@Nullable final Collection<Project> collection);

    @Insert("INSERT INTO tm_project (id, name, description, user_id, status, created, start_dt, end_dt) " +
            " VALUES (#{id}, #{name}, #{description}, #{userId}, #{status}, #{created}, #{dateBegin}, #{dateEnd})")
    void addByUserId(@Param("userId") @Nullable String userId, @Nullable Project model);

    @Update("UPDATE tm_project SET name = #{name}, description = #{description}, user_id = #{userId}, " +
            "status = #{status}, created = #{created}, start_dt = #{dateBegin}, end_dt = #{dateEnd} WHERE id = #{id}")
    int update(@NotNull Project project);

    @NotNull
    @Select("SELECT id, name, description, user_id, status, created, start_dt, end_dt FROM tm_project")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "dateBegin", column = "start_dt"),
            @Result(property = "dateEnd", column = "end_dt")
    })
    List<Project> findAll();

    @NotNull
    @Select("SELECT id, name, description, user_id, status, created, start_dt, end_dt FROM tm_project ORDER BY #{sort}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "dateBegin", column = "start_dt"),
            @Result(property = "dateEnd", column = "end_dt")
    })
    List<Project> findAllSort(@Param("sort") @NotNull Sort sort);

    @NotNull
    @Select("SELECT id, name, description, user_id, status, created, start_dt, end_dt FROM tm_project " +
            "WHERE user_id = #{userId} ORDER BY #{sort}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "dateBegin", column = "start_dt"),
            @Result(property = "dateEnd", column = "end_dt")
    })
    List<Project> findAllSortByUserId(@Param("userId") @Nullable String userId, @Param("sort") @Nullable Sort sort);

    @Delete("DELETE FROM tm_project")
    void clear();

    @Delete("DELETE FROM tm_project WHERE USER_ID = #{userId}")
    void clearByUserId(@Param("userId") @NotNull String userId);

    boolean existsById(@Nullable String id);

    boolean existsByIdUserId(@NotNull String userId, @NotNull String id);

    @Nullable
    @Select("SELECT id, name, description, user_id, status, created, start_dt, end_dt FROM tm_project " +
            "WHERE id = #{id} LIMIT 1")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "dateBegin", column = "start_dt"),
            @Result(property = "dateEnd", column = "end_dt")
    })
    Project findOneById(@Param("userId") @Nullable String id);

    @Nullable
    @Select("SELECT id, name, description, user_id, status, created, start_dt, end_dt FROM tm_project " +
            "WHERE id = #{id} AND user_id = #{userId} LIMIT 1")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "dateBegin", column = "start_dt"),
            @Result(property = "dateEnd", column = "end_dt")
    })
    Project findOneByIdUserId(@Param("userId") @Nullable String userId, @Param("id") @Nullable String id);

    @Select("SELECT id, name, description, user_id, status, created, start_dt, end_dt FROM tm_project LIMIT 1 OFFSET #{index}")
    Project findOneByIndex(@Param("index") @Nullable Integer index);

    @Nullable
    @Select("SELECT id, name, description, user_id, status, created, start_dt, end_dt FROM tm_project " +
            "WHERE user_id = #{userId} LIMIT 1 OFFSET #{index}")
    Project findOneByIndexByUserId(@Param("userId") @Nullable String userId, @Param("index") @Nullable Integer index);

    @Delete("DELETE FROM tm_project WHERE id = #{id}")
    int remove(@Nullable Project model);

    @Delete("DELETE FROM tm_project WHERE user_id = #{userId}")
    int removeByUserId(@Param("userId") @Nullable String userId, @Nullable Project model);

    @Delete("DELETE FROM tm_project WHERE user_id = #{userId} AND id = #{id}")
    void removeAll(@Nullable Collection<Project> collection);

    @Select("SELECT COUNT(1) FROM tm_project")
    long count();

    @Select("SELECT COUNT(1) FROM tm_project WHERE USER_ID = #{userId}")
    long countByUserId(@Param("userId") @Nullable String userId);
}
